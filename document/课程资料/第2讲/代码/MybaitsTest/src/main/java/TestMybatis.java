import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import com.youfan.entity.Person;

public class TestMybatis {
   public SqlSessionFactory getfactory() throws IOException{
	   String filepath = "SqlMappingConfig.xml";
	   InputStream in = Resources.getResourceAsStream(filepath);
	   SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(in);
	   return sqlSessionFactory;
   }
   
   @Test
   public void testinsert() throws IOException{
	   SqlSessionFactory sqlSessionFactory = this.getfactory();
	   SqlSession sqlsession = sqlSessionFactory.openSession();
	   Person person = new Person();
	   person.setName("小高");
	   person.setAddress("上海");
	   person.setAge(15);
	   person.setBirthday("05-04");
	   sqlsession.insert("test1.inserperson", person);
	   System.out.println("id "+person.getId());
	   sqlsession.commit();
	   sqlsession.close();
   }
   
   @Test
   public void testquerybyid() throws IOException{
	   SqlSessionFactory sqlSessionFactory = this.getfactory();
	   SqlSession sqlsession = sqlSessionFactory.openSession();
	   Person person = sqlsession.selectOne("querypersonbyid", 1);
	   System.out.println(person);
	   sqlsession.close();
   }
   
   @Test
   public void testquerybyname() throws IOException{
	   SqlSessionFactory sqlSessionFactory = this.getfactory();
	   SqlSession sqlsession = sqlSessionFactory.openSession();
	   List<Person> personlist = sqlsession.selectList("querypersonbyname", "友凡");
	   for(int i=0;i<personlist.size();i++){
		   System.out.println(personlist.get(i));
	   }
	   sqlsession.close();
   }
   
   @Test
   public void testdeletebyid() throws IOException{
	   SqlSessionFactory sqlSessionFactory = this.getfactory();
	   SqlSession sqlsession = sqlSessionFactory.openSession();
	   sqlsession.delete("deletepersonbyid", 1);
	   sqlsession.commit();
	   sqlsession.close();
   }
   
   @Test
   public void testupdatePerson() throws IOException{
	   SqlSessionFactory sqlSessionFactory = this.getfactory();
	   SqlSession sqlsession = sqlSessionFactory.openSession();
	   Person person = new Person();
	   person.setId(2);
	   person.setName("有范");
	   person.setAddress("北京");
	   person.setAge(15);
	   person.setBirthday("06-04");
	   sqlsession.update("updatepersonbyid", person);
	   sqlsession.commit();
	   sqlsession.close();
   }
   
   
   
}
