<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="/YF_MS_WEB/orderAction/topayorder">
购买数量：<input type="text" name="num" ><span id="remainnoties"></span>
<input type="hidden" name="id" value="${msproduct.id}">
<table border="1">
	<tr>
		<td>商品标题</td>
		<td>商品图片</td>
		<td>秒杀价格</td>
		<td>秒杀原价</td>
		<td>秒杀开始时间</td>
		<td>秒杀结束时间</td>
		<td>秒杀商品数</td>
		<td>库存</td>
	</tr>
	<tr>
		<td>${msproduct.producttitle}</td>
		<td>${msproduct.productpicture}</td>
		<td>${msproduct.miaoshaprice}</td>
		<td>${msproduct.originalprice}</td>
		<td><span id="starttime"><fmt:formatDate value="${msproduct.starttime}" pattern="yyyy-MM-dd HH:mm:dd"/></span></td>
		<td><span id="endtime"><fmt:formatDate value="${msproduct.endtime}" pattern="yyyy-MM-dd HH:mm:dd"/></span></td>
		<td>${msproduct.productcount}</td>
		<td>${msproduct.stockcount}</td>
	</tr>
</table>
<table border="1">
<tr>
		<td>商品产地</td>
		<td>商品名称</td>
		<td>商品品牌</td>
		<td>商品重量</td>
		<td>规格和包装</td>
		<td>商品详情图片地址</td>
	</tr>
	<tr>
		<td>${msproductdetail.productplace}</td>
		<td>${msproductdetail.productname}</td>
		<td>${msproductdetail.brandname}</td>
		<td>${msproductdetail.productweight}</td>
		<td>${msproductdetail.specification}</td>
		<td>${msproductdetail.productdetailpicture}</td>
	</tr>
</table>
<input id="sellbnt" type="button" value="立即购买" onclick="submit(this)"/>
</form>
</body>
<script type="text/javascript" src="/YF_MS_WEB/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
	function remaintime(){
		var starttime = $("#starttime").html();
		var s1 = new Date(starttime.replace("/-/g","/"));
		var s2 = new Date();
		var date3 = s1.getTime() - s2.getTime();//这是一个相差时间戳
		if(date3 > 2){
			$("#sellbnt").attr({"disabled":"disabled"});
			//天
			var days = Math.floor(date3/(24*3600*1000));
			//小时
			var leave = date3%(24*3600*1000)
			var hours = Math.floor(leave/(3600*1000));
			//分钟
			var leave1 = leave%(3600*1000)
			var minutes = Math.floor(leave1/(60*1000));
			//秒
			var leave2 = leave1%(60*1000)
			var seconds = Math.floor(leave2/1000)
			$("#remainnoties").html("相差 "+days+" 天 "+ hours + " 小时" + minutes + " 分钟"+seconds+"秒");
		}else{
			$("#remainnoties").html("");
			$("#sellbnt").removeAttr("disabled");
		}
		
		//alert("相差 "+days+" 天 "+ hours + " 小时" + minutes + " 分钟"+seconds+"秒");
	}
	setInterval('remaintime()',500);
	
	
</script>
<script type="text/javascript">
function submit(obj){
	obj.parent.sumbit();
}
</script>
</html>