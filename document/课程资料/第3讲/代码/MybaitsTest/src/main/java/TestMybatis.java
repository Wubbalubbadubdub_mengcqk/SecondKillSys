import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import com.youfan.entity.Person;
import com.youfan.mapper.PersonMapper;
import com.youfan.param.CustomPerson;
import com.youfan.vo.PersonVo;

public class TestMybatis {
   public SqlSessionFactory getfactory() throws IOException{
	   String filepath = "SqlMappingConfig.xml";
	   InputStream in = Resources.getResourceAsStream(filepath);
	   SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(in);
	   return sqlSessionFactory;
   }
   
   @Test
   public void testinsert() throws IOException{
	   SqlSessionFactory sqlSessionFactory = this.getfactory();
	   SqlSession sqlsession = sqlSessionFactory.openSession();
	   PersonMapper personMapper = sqlsession.getMapper(PersonMapper.class);
	   Person person = new Person();
	   person.setName("小白");
	   person.setAddress("上海");
	   person.setAge(15);
	   person.setBirthday("05-04");
	   personMapper.inserperson(person);
	   System.out.println("id "+person.getId());
	   sqlsession.commit();
	   sqlsession.close();
   }
   
   @Test
   public void testquerybyid() throws IOException{
	   SqlSessionFactory sqlSessionFactory = this.getfactory();
	   SqlSession sqlsession = sqlSessionFactory.openSession();
	   PersonMapper personMapper = sqlsession.getMapper(PersonMapper.class);
	   Person person = personMapper.querypersonbyid(2);
	   System.out.println(person);
	   sqlsession.close();
   }
   
   @Test
   public void testquerybyname() throws IOException{
	   SqlSessionFactory sqlSessionFactory = this.getfactory();
	   SqlSession sqlsession = sqlSessionFactory.openSession();
	   PersonMapper personMapper = sqlsession.getMapper(PersonMapper.class);
	   List<Person> personlist = personMapper.querypersonbyname("小");
	   for(int i=0;i<personlist.size();i++){
		   System.out.println(personlist.get(i));
	   }
	   sqlsession.close();
   }
   
   @Test
   public void testdeletebyid() throws IOException{
	   SqlSessionFactory sqlSessionFactory = this.getfactory();
	   SqlSession sqlsession = sqlSessionFactory.openSession();
	   PersonMapper personMapper = sqlsession.getMapper(PersonMapper.class);
	   personMapper.deletepersonbyid(2);
	   sqlsession.commit();
	   sqlsession.close();
   }
   
   @Test
   public void testupdatePerson() throws IOException{
	   SqlSessionFactory sqlSessionFactory = this.getfactory();
	   SqlSession sqlsession = sqlSessionFactory.openSession();
	   PersonMapper personMapper = sqlsession.getMapper(PersonMapper.class);
	   Person person = new Person();
	   person.setId(3);
	   person.setName("友凡");
	   person.setAddress("北京");
	   person.setAge(15);
	   person.setBirthday("07-04");
	   personMapper.updatepersonbyid(person);
	   sqlsession.commit();
	   sqlsession.close();
   }
   
   @Test
   public void testquerypersonbyvo() throws IOException{
	   SqlSessionFactory sqlSessionFactory = this.getfactory();
	   SqlSession sqlsession = sqlSessionFactory.openSession();
	   PersonMapper personMapper = sqlsession.getMapper(PersonMapper.class);
	   PersonVo personVo = new PersonVo();
	   CustomPerson customPerson = new CustomPerson();
	   customPerson.setName("友凡");
	   customPerson.setBirthday("07-04");
	   personVo.setCustomPerson(customPerson);
	   List<Person> personlist = personMapper.querypersonbyvo(personVo);
	   for(int i=0;i<personlist.size();i++){
		   System.out.println(personlist.get(i));
	   }
	   sqlsession.close();
   }
   
   
   
}
