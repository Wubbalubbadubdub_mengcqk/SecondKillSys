package com.youfan.mapper;

import java.util.List;

import com.youfan.entity.Person;
import com.youfan.vo.PersonVo;

public interface PersonMapper {
	public Person querypersonbyid(int id);
	public List<Person> querypersonbyname(String name);
	public void inserperson(Person person);
	public void deletepersonbyid(int id);
	public void updatepersonbyid(Person person);
	public List<Person> querypersonbyvo(PersonVo personVo);
}
