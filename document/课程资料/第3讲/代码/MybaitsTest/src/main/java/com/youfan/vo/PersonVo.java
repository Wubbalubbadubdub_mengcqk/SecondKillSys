package com.youfan.vo;

import com.youfan.param.CustomPerson;

public class PersonVo {
	private CustomPerson customPerson;

	public CustomPerson getCustomPerson() {
		return customPerson;
	}

	public void setCustomPerson(CustomPerson customPerson) {
		this.customPerson = customPerson;
	}
	
}
