package com.youfan.control;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("hello")
public class HelloControl {
	
	@RequestMapping("test")
	public void hello(HttpServletRequest request,HttpServletResponse response){
		String name = request.getParameter("name");
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		try {
			PrintWriter printwriter = response.getWriter();
			printwriter.println("hello=="+name);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
