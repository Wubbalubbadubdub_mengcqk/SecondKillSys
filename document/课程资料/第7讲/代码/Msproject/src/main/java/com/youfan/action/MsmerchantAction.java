package com.youfan.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import com.youfan.enity.Msmerchant;
import com.youfan.service.MsmerchantService;
import com.youfan.vo.MsmerchantVo;

@Controller
@RequestMapping("msmerchantAction")
public class MsmerchantAction {
	
	@Autowired
	MsmerchantService msmerchantService;
	

	@RequestMapping("toadd")
	public String toadd(){
		return "msmerchant/add";
	}
	
	@RequestMapping(value = "add")
	public void add(Msmerchant msmerchant){
		msmerchantService.insertMsmerchant(msmerchant);
		System.out.println(msmerchant);
	}
	
	@RequestMapping("toupdate")
	public String toupdate(HttpServletRequest request,int id){
		Msmerchant msmerchant = msmerchantService.queryMsmerchantByid(id);
		request.setAttribute("msmerchant", msmerchant);
		return "msmerchant/update";
	}
	
	@RequestMapping("update")
	public void update(HttpServletRequest request,Msmerchant msmerchant){
		msmerchantService.updateMsmerchant(msmerchant);
		System.out.println(msmerchant);
	}
	
	@RequestMapping("del")
	public void del(HttpServletRequest request,int id){
		msmerchantService.deleteMsmerchantbyid(id);
	}
	
	@RequestMapping("querybyid")
	public String querybyid(HttpServletRequest request,int id){
		Msmerchant msmerchant = msmerchantService.queryMsmerchantByid(id);
		request.setAttribute("msmerchant", msmerchant);
		return "msmerchant/view";
	}
	
	@RequestMapping("querybyvo")
	public String querybyvo(HttpServletRequest request,MsmerchantVo msmerchantVo){
		List<Msmerchant> list = msmerchantService.queryMsmerchantbyvo(msmerchantVo);
		request.setAttribute("msmerchantlist", list);
		return "msmerchant/list";
	}
	
	
	
	
	
}
