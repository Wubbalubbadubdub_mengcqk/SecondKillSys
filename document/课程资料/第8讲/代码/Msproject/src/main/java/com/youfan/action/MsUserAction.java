package com.youfan.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import com.youfan.enity.Msmerchant;
import com.youfan.enity.Msuser;
import com.youfan.service.MsUserService;
import com.youfan.service.MsmerchantService;
import com.youfan.vo.msmerchant.MsmerchantVo;
import com.youfan.vo.msuser.MsuserVo;

@Controller
@RequestMapping("msUserAction")
public class MsUserAction {
	
	@Autowired
	MsUserService msUserService;
	

	@RequestMapping("toadd")
	public String toadd(){
		return "user/add";
	}
	
	@RequestMapping(value = "add")
	public void add(Msuser msuser){
		msUserService.insertUser(msuser);
		System.out.println(msuser);
	}
	
	@RequestMapping("toupdate")
	public String toupdate(HttpServletRequest request,int id){
		Msuser user = msUserService.queryMsuserByid(id);
		request.setAttribute("msuser",user);
		return "user/update";
	}
	
	@RequestMapping("update")
	public void update(HttpServletRequest request,Msuser msuser){
		msUserService.updateMsuser(msuser);
		System.out.println(msuser);
	}
	
	@RequestMapping("del")
	public void del(HttpServletRequest request,int id){
		msUserService.deleteMsuserbyid(id);
	}
	
	@RequestMapping("querybyid")
	public String querybyid(HttpServletRequest request,int id){
		Msuser msuser = msUserService.queryMsuserByid(id);
		request.setAttribute("msuser", msuser);
		return "user/view";
	}
	
	@RequestMapping("querybyvo")
	public String querybyvo(HttpServletRequest request,MsuserVo msuserVo){
		List<Msuser> list = msUserService.queryMsuserbyvo(msuserVo);
		request.setAttribute("msuserlist", list);
		return "user/list";
	}
	
	
	
	
	
}
