package com.second.kill.controller;

import com.second.kill.serv.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/my")
public class Test {

    @Autowired
    private TestService testService;

    @ResponseBody
    @RequestMapping("/abc")
    public Object a(){
        System.out.println(testService.query());
        return testService.query();
    }
}
