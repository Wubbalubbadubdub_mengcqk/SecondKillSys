package com.second.kill.dao;

import java.util.Map;

public interface TestMapper {

    Map<String,Object> query();

}
