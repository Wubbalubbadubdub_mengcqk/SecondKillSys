package com.second.kill.serv;

import com.second.kill.dao.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class TestService {

    @Autowired
    private TestMapper testMapper;

    public Map<String,Object> query(){
        return testMapper.query();
    }
}
